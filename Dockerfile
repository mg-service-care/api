FROM gradle:5.4.1-jdk11 AS builder

USER root
RUN apt-get update && apt-get install -y git nano tree
USER gradle

COPY --chown=gradle:gradle ./ /home/gradle/package
WORKDIR /home/gradle/package

RUN gradle versionDisplay versionFile bootJar && cat api/build/tmp/bootJar/MANIFEST.MF

ARG run_sonarqube
RUN if [ -n "$run_sonarqube" ]; then \
  touch ../gradle.properties; \
  cp ../gradle.properties /home/gradle/.gradle/gradle.properties; \
  gradle sonarqube; \
fi

FROM openjdk:11-jre-stretch
EXPOSE 8080
COPY --from=builder /home/gradle/package/api/build/libs/api-0.1.0.jar /app/
COPY --from=builder /home/gradle/package/api/build/version.properties /app/
COPY --from=builder /home/gradle/package/api/build/tmp/bootJar/MANIFEST.MF /app/
WORKDIR /app
CMD ["java", "-jar", "api-0.1.0.jar"]
