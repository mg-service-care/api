package api.users;

import api.vehicles.Vehicle;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;
import java.util.List;

@Projection(name = "withVehicles", types = { User.class })
public interface UserWithVehiclesProjection {
    String getName();
    LocalDateTime getCreatedAt();
    LocalDateTime getUpdatedAt();
    List<Vehicle> getVehicles();
}
