package api.places;

import api.vehicles.Vehicle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "places", path = "places")
public interface PlaceRepository extends PagingAndSortingRepository<Place, Long> {
    List<Place> findTop3ByVehicleOrderByCreatedAtAsc(@Param("vehicle") Vehicle vehicle);
}
