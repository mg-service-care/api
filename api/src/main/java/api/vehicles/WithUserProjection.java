package api.vehicles;

import api.users.User;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;

@Projection(name = "withUser", types = { Vehicle.class })
public interface WithUserProjection {
    String getLicensePlate();
    LocalDateTime getCreatedAt();
    LocalDateTime getUpdatedAt();

    User getUser();
}
