# API application

This project is part of Service Care ecosystem and is used to store and fetch
last geo positions of truck drivers.

## Technology

This project is made with:

- docker >= 18.09.6
- docker-compose >= 1.24.0
- java = 11
- gradle >= 5.4.1
- SpringBoot framework 2.1.5

## Starting the application

This project is made over docker structure. So, to start the application its
simple as run a single line in terminal.

```bash
docker-compose up
```

If is more convenient to developer work with all dependencies in local mode
there's and approach using gradle build system. The developer can use the tasks
of gradle to build or start the application.

```bash
gradle bootRun
```

## Database

The chosen database to store the application data is mysql or any other
relational database.

In past was thought in use a NoSQL option, like mongoDB or elastic search
because of it really simple to use geo locations search using features provided
by that kind of databases. But when looked at nature of project, the search by
nearly feature will not be used at first time, so it's better to use a simple
relational database storing the user in one table and the received positions in
other table.

Above, the entity-relationship model of required database:

```plain
.------------.        .---------------.          .------------.
| Users      |        | Vehicles      |          | Places     |
+------------+        +---------------+          +------------+
| user_id    |1---.   | vehicle_id    |1---.     | place_id   |
| name       |    '--N| user_id       |    '----N| vehicle_id |
| created_at |        | license_plate |          | lat        |
| updated_at |        | created_at    |          | long       |
'------------'        | updated_at    |          | created_at |
                      '---------------'          | updated_at |
                                                 '------------'
```

## Responsible

- Michael Granados
