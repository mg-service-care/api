CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id`    INTEGER UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id`       INTEGER UNSIGNED NOT NULL,
  `license_plate` VARCHAR(20) NOT NULL,
  `created_at`    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

  INDEX `user_index` (`user_id`),
  INDEX `license_plate_index` (`license_plate`),
  FOREIGN KEY (`user_id`)
    REFERENCES `users`(`user_id`)
    ON UPDATE CASCADE ON DELETE RESTRICT
);
