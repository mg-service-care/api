CREATE TABLE IF NOT EXISTS `places` (
  `place_id`   INTEGER UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `vehicle_id` INTEGER UNSIGNED NOT NULL,
  `latitude`   FLOAT NOT NULL,
  `longitude`  FLOAT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),

  INDEX `vehicle_index` (`vehicle_id`),
  FOREIGN KEY (`vehicle_id`)
    REFERENCES `vehicles`(`vehicle_id`)
    ON UPDATE CASCADE ON DELETE RESTRICT
);
